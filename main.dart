import 'dart:async';

import 'datetoday.dart';
import 'helloworld.dart';
import 'goodbyeworld.dart';

const timeout = const Duration(seconds: 20);
const ms = const Duration(milliseconds: 1);

void main(){
  helloWorld();
  defineDate();
  goodByeWorld();
}