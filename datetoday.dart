defineDate() {
 var now = new DateTime.now();
 var year = now.year;
 var day = now.day;
 var mnth = now.month;
 var mnthText = defineMonth(mnth);
 print('\tToday is ${day}th of ${mnthText}.\n\tCurrent year — ${year}.\n\tStay tuned for more informative programs <3');
}

defineMonth(int mnth) {
  List<String> mnths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  //print(mnths.length);
  return mnths[mnth - 1];
}

main(){
 defineDate();
}